#!/bin/sh

VER=2.8.4

make clean
cd ..
if ! [ -h greenlight-${VER} ]; then
	ln -s greenlight greenlight-${VER}
fi
tar \
	--exclude=debian \
	--exclude=.git \
	--exclude=.env \
	-hzcf greenlight_${VER}.orig.tar.gz \
	greenlight-${VER}
