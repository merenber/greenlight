prefix = /opt/greenlight

RUBY_VERSION = 2.7.0
BUNDLE = $(prefix)/.gem/ruby/$(RUBY_VERSION)/bin/bundle

all:	export HOME = $(prefix)
all:
	[ -x $(BUNDLE) ] || gem install --user-install bundler
	$(BUNDLE) config --global frozen 1
	$(BUNDLE) install --deployment --without development:test:assets -j4 --path=vendor/bundle
	rm -rf vendor/bundle/ruby/$(RUBY_VERSION)/cache/*.gem
	find vendor/bundle/ruby/$(RUBY_VERSION)/gems/ -name "*.c" -delete
	find vendor/bundle/ruby/$(RUBY_VERSION)/gems/ -name "*.o" -delete

install:
	cp -r app bin .bundle config config.ru db Gemfile Gemfile.lock greenlight.nginx \
		lib log public Rakefile .rspec .rubocop.yml .ruby-version sample.env \
		scripts spec vendor $(DESTDIR)$(prefix)/

clean:
	rm -rf vendor/bundle .bundle public/assets log/*.log node_modules package-lock.json
